import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Activity

        Scanner in = new Scanner(System.in);

        //try-catch

        try{

            System.out.println("Input an integer whose factorial will be computed:");
            int num = in.nextInt();

            int answer = 1;
            int counter = 1;

            //While Loop

            while(answer<=num){
                counter=counter*answer;
                answer++;
            }


            //For-Loop

            for(int i=1;i<=num;i++){
                counter=counter*i;
            }

            if (num <= 0) {
                System.out.println("Negative numbers and zero are not allowed.");
            } else {
                System.out.println("The factorial of " + num + " is " + counter);
            }



        }catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }






    }
}